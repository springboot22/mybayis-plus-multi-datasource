1、同一个连接，不同的库
在数据库url配置上面，不写库名。
每一次调用Dao方法时手动进行use database。
同时需要保证 use database需要和后面一系列操作在同一个事务中，否则无法保证use database和后面的操作中是否会有连接被回收后，操作不在同一个库的情况。
2、不同的连接地址
这个时候就必须进行多个url的配置,此项目是做了在多数据源的情况下，期望能动动态注册一个数据源的实现方式，可以通过调用接口的方式进行数据源的新增
其实也可以自己继承AbstractDataSource 自己仿照AbstractRoutingDataSource的实现来进行处理
