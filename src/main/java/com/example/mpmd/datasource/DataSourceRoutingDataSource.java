package com.example.mpmd.datasource;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * @author will.tuo
 * @date 2023/2/22 9:02
 * @description DataSource路由，通过DynamicDataSourceContextHolder获取当前的线程绑定的数据源
 */

public class DataSourceRoutingDataSource extends AbstractRoutingDataSource {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    protected Object determineCurrentLookupKey() {
        Object dataSourceKey = DynamicDataSourceContextHolder.getDataSourceKey();
        logger.info("Current DataSource is [{}]", dataSourceKey);
        return dataSourceKey;
    }
}
