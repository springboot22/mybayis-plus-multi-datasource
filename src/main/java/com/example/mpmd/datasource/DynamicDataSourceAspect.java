package com.example.mpmd.datasource;

import com.baomidou.dynamic.datasource.annotation.DS;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author will.tuo
 * @date 2023/2/22 10:02
 * @description 通过
 */
@Aspect
@Order(value = -1)
@Component
public class DynamicDataSourceAspect {

    private static final Logger logger = LoggerFactory.getLogger(DynamicDataSourceAspect.class);

    @Pointcut("@annotation(com.baomidou.dynamic.datasource.annotation.DS)")
    public void daoAspect() {
    }

    @Before("daoAspect()  && @annotation(ds)")
    public void switchDataSource(DS ds) {
        String value = ds.value();
        boolean containDataSourceKey = DynamicDataSourceContextHolder.containDataSourceKey(value);
        if (!containDataSourceKey) {
            throw new IllegalStateException("数据源[" + value + "]不存在");
        }
        DynamicDataSourceContextHolder.setDataSourceKey(value);
        logger.info("DataSource set to " + value);
    }

    @After("daoAspect()")
    public void restoreDataSource(JoinPoint point) {
        DynamicDataSourceContextHolder.clearDataSourceKey();
        logger.info("Restore DataSource to [{}] in Method [{}]",
                DynamicDataSourceContextHolder.getDataSourceKey(), point.getSignature());
    }

}
