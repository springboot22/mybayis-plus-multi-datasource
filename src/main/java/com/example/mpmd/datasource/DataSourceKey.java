package com.example.mpmd.datasource;

/**
 * @author will.tuo
 * @date 2023/2/22 10:00
 * @description 多数据源的枚举，事实上我们是直接使用的字符串作为一个数据源Map的key进行使用的
 */
public enum DataSourceKey {
    master,
    slave
}
