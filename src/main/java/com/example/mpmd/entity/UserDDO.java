package com.example.mpmd.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author will.tuo
 * @date 2023/2/22 10:26
 * @description 无
 */
@TableName("user")
@Data
public class UserDDO {

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    private String name;
}
