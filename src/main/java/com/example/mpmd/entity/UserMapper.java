package com.example.mpmd.entity;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author will.tuo
 * @date 2023/2/22 10:26
 * @description 无
 */
@Mapper
@Repository
public interface UserMapper extends BaseMapper<UserDDO> {

}
