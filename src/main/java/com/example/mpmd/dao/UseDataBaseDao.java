package com.example.mpmd.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Repository
public interface UseDataBaseDao {
    /**
     * 同一个连接下需要切换数据库的情况
     *
     * @param dbName
     */
    @Select("use ${dbName}")
    void useDatabase(@Param("dbName") String dbName);
}
