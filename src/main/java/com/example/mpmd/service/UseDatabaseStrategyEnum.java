package com.example.mpmd.service;

public enum UseDatabaseStrategyEnum implements UseDatabaseStrategy{
    DefaultUseDatabaseStrategy() {
        int count = 0;
        @Override
        public String getDatabaseName() {
            return count++%2 == 0 ?"world2" : "world";
        }
    }
}
