package com.example.mpmd.service;

public interface UseDatabaseStrategy {
    String getDatabaseName();
}
