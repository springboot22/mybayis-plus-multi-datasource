package com.example.mpmd.service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.example.mpmd.aop.UseDatabase;
import com.example.mpmd.dao.UseDataBaseDao;
import com.example.mpmd.entity.UserDDO;
import com.example.mpmd.entity.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author will.tuo
 * @date 2023/2/22 10:28
 * @description 无
 */
@Service
public class UserServiceImpl {

    @Autowired
    UserMapper userDao;

    @Autowired
    UseDataBaseDao useDataBaseDao;

    @DS("master")
    @Transactional
    public Object get() {
        UserDDO userDDO = userDao.selectById(1);
        System.out.println(userDDO.toString());
        return userDDO;
    }

    @DS("slave")
    @Transactional
    public Object get2() {
        UserDDO userDDO = userDao.selectById(1);
        System.out.println(userDDO.toString());
        return userDDO;
    }

    @Transactional
    @UseDatabase
    public Object get3(){
        UserDDO userDDO = userDao.selectById(1);
        System.out.println(userDDO);
        return userDDO;
    }

}
