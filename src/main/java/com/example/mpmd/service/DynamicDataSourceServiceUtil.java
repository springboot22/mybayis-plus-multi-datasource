package com.example.mpmd.service;

import com.baomidou.dynamic.datasource.creator.DefaultDataSourceCreator;
import com.baomidou.dynamic.datasource.spring.boot.autoconfigure.DataSourceProperty;
import com.example.mpmd.datasource.DataSourceRoutingDataSource;
import com.example.mpmd.datasource.DynamicDataSourceContextHolder;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author will.tuo
 * @date 2023/2/22 15:42
 * @description 动态添加数据源
 */
@Component
public class DynamicDataSourceServiceUtil {

    @Autowired
    DataSourceRoutingDataSource dynamicRoutingDataSource;

    @Autowired
    private DefaultDataSourceCreator defaultDataSourceCreator;

    public void addDataSource(String name, DataSourceProperty dataSourceProperty) {
        boolean exist = DynamicDataSourceContextHolder.containDataSourceKey(name);
        if (exist) {
            return;
        }
        Map<Object, DataSource> dataSources = dynamicRoutingDataSource.getResolvedDataSources();
        HashMap<Object, Object> objectObjectHashMap = new HashMap<>(dataSources);
        objectObjectHashMap.put(name, defaultDataSourceCreator.createDataSource(dataSourceProperty));
        dynamicRoutingDataSource.setTargetDataSources(objectObjectHashMap);
        dynamicRoutingDataSource.afterPropertiesSet();
        DynamicDataSourceContextHolder.dataSourceKeys.add(name);
    }
}
