package com.example.mpmd.controller;

import com.baomidou.dynamic.datasource.spring.boot.autoconfigure.DataSourceProperty;
import com.example.mpmd.service.DynamicDataSourceServiceUtil;
import com.example.mpmd.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author will.tuo
 * @date 2023/2/22 10:57
 * @description 无
 */
@RestController
public class MultiController {

    @Autowired
    UserServiceImpl userService;

    @Autowired
    DynamicDataSourceServiceUtil dynamicDataSourceService;


    @GetMapping("/1")
    public Object a() {
        return userService.get();
    }

    @GetMapping("/2")
    public Object b() {
        return userService.get3();
    }

    @GetMapping("/createDs")
    public Object c() {
        DataSourceProperty dataSourceProperty = new DataSourceProperty();
        dataSourceProperty.setPassword("123456");
        dataSourceProperty.setUrl("jdbc:mysql://127.0.0.1:3306/world2");
        dataSourceProperty.setUsername("root");
        dataSourceProperty.setDriverClassName("com.mysql.cj.jdbc.Driver");
        dataSourceProperty.setPoolName("slave");
        dynamicDataSourceService.addDataSource("slave", dataSourceProperty);
        return userService.get2();
    }

}
