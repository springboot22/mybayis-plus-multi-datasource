package com.example.mpmd;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan({"com.example.mpmd"})
public class MyBayisPlusMultiDataSourceApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyBayisPlusMultiDataSourceApplication.class, args);
    }

}
