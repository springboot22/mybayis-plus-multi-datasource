package com.example.mpmd.aop;

import com.example.mpmd.service.UseDatabaseStrategyEnum;

import java.lang.annotation.*;

@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface UseDatabase {
    UseDatabaseStrategyEnum type() default UseDatabaseStrategyEnum.DefaultUseDatabaseStrategy;
}
