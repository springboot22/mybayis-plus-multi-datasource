package com.example.mpmd.aop;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.example.mpmd.dao.UseDataBaseDao;
import com.example.mpmd.datasource.DynamicDataSourceAspect;
import com.example.mpmd.datasource.DynamicDataSourceContextHolder;
import com.example.mpmd.service.UseDatabaseStrategy;
import com.example.mpmd.service.UseDatabaseStrategyEnum;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class UseDatabaseAspect {
    private static final Logger logger = LoggerFactory.getLogger(DynamicDataSourceAspect.class);

    @Autowired
    UseDataBaseDao useDataBaseDao;

    @Pointcut("@annotation(com.example.mpmd.aop.UseDatabase)")
    public void daoAspect() {
    }

    @Before("daoAspect()  && @annotation(ds)")
    public void switchDataSource(UseDatabase ds) {
        UseDatabaseStrategyEnum type = ds.type();
        String databaseName = type.getDatabaseName();
        useDataBaseDao.useDatabase(databaseName);
    }
}
